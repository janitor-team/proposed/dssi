Source: dssi
Section: libdevel
Priority: optional
Maintainer: Debian Multimedia Maintainers <debian-multimedia@lists.debian.org>
Uploaders:
 Alessio Treglia <alessio@debian.org>,
 Jaromír Mikeš <mira.mikes@seznam.cz>,
 Ross Gammon <rossgammon@debian.org>
Build-Depends:
 debhelper-compat (= 12),
 ladspa-sdk,
 libjack-dev,
 liblo-dev,
 libasound2-dev [linux-any],
 libdssialsacompat-dev [!linux-any],
 libx11-dev,
 libsndfile1-dev,
 libsm-dev,
 libsamplerate0-dev,
Standards-Version: 4.4.1
Homepage: http://dssi.sourceforge.net/
Vcs-Git: https://salsa.debian.org/multimedia-team/dssi.git
Vcs-Browser: https://salsa.debian.org/multimedia-team/dssi
Rules-Requires-Root: no

Package: dssi-dev
Architecture: any
Depends: ladspa-sdk,
 libasound2-dev [linux-any],
 libdssialsacompat-dev [!linux-any],
 pkg-config,
 ${misc:Depends}
Suggests: libjack-dev
Description: Header file for compiling DSSI plugins and hosts
 DSSI is an API for audio plugins, with particular application for software
 synthesis plugins with native user interfaces.
 .
 DSSI is an open specification developed for use in Linux audio applications,
 although it is portable to other platforms. It may be thought of as
 LADSPA-for-instruments, or something comparable to VSTi.
 .
 This package contains the header file required for compiling hosts and
 plugins.

Package: dssi-utils
Architecture: any
Section: sound
Depends: ${shlibs:Depends},
 ${misc:Depends}
Description: Command-line utilities for sending commands to DSSI plugins
 DSSI is an API for audio plugins, with particular application for software
 synthesis plugins with native user interfaces.
 .
 DSSI is an open specification developed for use in Linux audio applications,
 although it is portable to other platforms. It may be thought of as
 LADSPA-for-instruments, or something comparable to VSTi.
 .
 This package contains the dssi_osc_send and dssi_osc_update utilities which
 can, for instance, be used to send commands to the DSSI trivial synth
 plugin.

Package: dssi-host-jack
Architecture: any
Section: sound
Depends: ${shlibs:Depends},
 ${misc:Depends}
Recommends: dssi-utils
Description: Example of DSSI host
 DSSI is an API for audio plugins, with particular application for software
 synthesis plugins with native user interfaces.
 .
 DSSI is an open specification developed for use in Linux audio applications,
 although it is portable to other platforms. It may be thought of as
 LADSPA-for-instruments, or something comparable to VSTi.
 .
 This package contains an example DSSI host which is useful for testing new
 plugins. It listens for MIDI events on an ALSA sequencer port, delivers them
 to DSSI synths and outputs the result via JACK.  It does not currently support
 audio input (e.g. for DSSI effects plugins).

Package: dssi-example-plugins
Architecture: any
Section: sound
Depends: ${shlibs:Depends},
 ${misc:Depends}
Recommends: dssi-utils
Description: Examples of DSSI plugin
 DSSI is an API for audio plugins, with particular application for software
 synthesis plugins with native user interfaces.
 .
 DSSI is an open specification developed for use in Linux audio applications,
 although it is portable to other platforms. It may be thought of as
 LADSPA-for-instruments, or something comparable to VSTi.
 .
 This is a set containing three example DSSI plugins.
